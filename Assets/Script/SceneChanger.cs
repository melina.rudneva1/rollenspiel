﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{
    public void ChangeSceneOnClick(string scene) {
        SceneManager.LoadScene(scene);
    }

    public void ChangeSceneOnClickWithDelay(string scene) {
        StartCoroutine(ChangeSceneWithDelay(scene));
    }
    
    public IEnumerator ChangeSceneWithDelay(string scene) {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(scene);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
