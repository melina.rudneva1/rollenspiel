﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class AvatarSelector : MonoBehaviour
{
    public TMPro.TMP_Dropdown dropdownFK;
    public TMPro.TMP_Dropdown dropdownMA;
    public static Avatars avatarFK; // FK = Führungskraft
    public static Avatars avatarMA; //MA = Mitarbeiter(in)
    private int index; 

    public enum Avatars { Schildkröte, Löwe, Hase, Fuchs };

    public void Start() {
        string[] enumNames = Enum.GetNames(typeof(Avatars));
        List<string> names = new List <string>(enumNames);
        dropdownFK.AddOptions(names);
        dropdownMA.AddOptions(names);
    }

    public void Update() {
        ChooseFK();
        ChooseMA();
    }

    private void ChooseFK() {
        index = dropdownFK.value;
        AvatarSelector.avatarFK = (Avatars)index;
        Debug.Log("Führungskraft ist "+avatarFK.ToString());
    }

    private void ChooseMA() {
        index = dropdownMA.value;
        AvatarSelector.avatarMA = (Avatars)index;
        Debug.Log("Mitarbeiterin ist "+avatarMA.ToString());
    }

}
