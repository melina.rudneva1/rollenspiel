﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarDisplay : MonoBehaviour {

    private AvatarSelector.Avatars avatarFK;
    public GameObject[] avatarsFK;
    private AvatarSelector.Avatars avatarMA;
    public GameObject[] avatarsMA;

    void Start() {
        avatarFK = AvatarSelector.avatarFK;
        foreach (GameObject avatar in avatarsFK) {
            if (avatar.name != avatarFK.ToString()+"FK") avatar.SetActive(false);
        }
       
       avatarMA = AvatarSelector.avatarMA;
        foreach (GameObject avatar in avatarsMA) {
            if (avatar.name != avatarMA.ToString()+"MA") avatar.SetActive(false);
        }

        if (avatarsMA.Length!=0) {NotTalkingMA();}
        NotTalkingFK();
    }

    public static void HideMA() {
        GameObject.Find(AvatarSelector.avatarMA+"MA").SetActive(false);
    }

    public static void NotTalkingMA() {
        GameObject.Find(AvatarSelector.avatarMA+"MA").GetComponent<Animator>().enabled = false;
    }

    public static void TalkingMA() {
        GameObject.Find(AvatarSelector.avatarMA+"MA").GetComponent<Animator>().enabled = true;
    }

    public static void NotTalkingFK() {
        GameObject.Find(AvatarSelector.avatarFK+"FK").GetComponent<Animator>().enabled = false;
    }

    public static void TalkingFK() {
        GameObject.Find(AvatarSelector.avatarFK+"FK").GetComponent<Animator>().enabled = true;
    }
    
}
