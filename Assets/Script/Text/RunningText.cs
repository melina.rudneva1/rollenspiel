﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RunningText : MonoBehaviour
{
    public TMP_Text textDisplay;
    private Queue<string> sentences;
    [TextArea(2,15)]
    public string[] monologue;

    void Start() {
        sentences = new Queue<string>();
        StartMonologue(monologue);
    }

    public void StartMonologue(string[] monologue) {
        sentences.Clear();
        foreach (string sentence in monologue) {
            sentences.Enqueue(sentence);
        }
       PrintNextSentence();
    }

    public void PrintNextSentence() {
        string sentence = sentences.Dequeue();
        textDisplay.text = "";
        StopAllCoroutines();
        StartCoroutine(TypeLetters(sentence));
    }

    public IEnumerator TypeLetters(string sentence) {
        foreach (char letter in sentence.ToCharArray()) {
            textDisplay.text += letter;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
