﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;
using TMPro;
using UnityEngine.SceneManagement;

//The concept of using JsonData in the dialogue is TAKEN FROM a youtube video called "DIALOGUE SYSTEM UNITY TUTORIAL #3 - Options / Branching"
//Source: https://www.youtube.com/watch?v=pxpS8i7awgM
public class MainDialogueManager : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public GameObject[] buttons;
    public GameObject tablet;
    
    private JsonData dialogue; 
    private int index;
    private string avatar;
    private bool inDialogue = false;
    private bool isChoosing = false;

    private void Start() {
        DeactivateButtons();
        DeactivateTablet();
        LoadDialogue("dialogues");
        string firstSentence = "Um zu beginnen und um das Gespräch weiterzuführen drücken Sie die Leertaste.";
        StopAllCoroutines();
        textDisplay.text = "";
        StartCoroutine(TypeLetters(firstSentence));      
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (!PrintNextSentence()) { 
                Debug.Log("End of dialogue"); 
                AvatarDisplay.HideMA();
                StopAllCoroutines();
                textDisplay.text = "Die Mitarbeiterin verlässt das Büro. Auf dem Tablet scheint eine Benachrichtigung auf: „Wir sind in Raum 003. Wir warten... 😉 “"; 
                StartCoroutine(ChangeSceneWithDelay("LoadingScene"));
            }
        }
    } 

    private void LoadDialogue(string path) {
        if (!inDialogue) {
            index = 0;
            var jsonTextFile = Resources.Load<TextAsset>("Dialogues/"+path);
            dialogue = JsonMapper.ToObject(jsonTextFile.text);
            inDialogue = true;
        }
    }

    private bool PrintNextSentence() {
        if (inDialogue) {
            JsonData line = dialogue[index];
            
            foreach (JsonData key in line.Keys) {
                avatar = key.ToString();
            }
            
            if (avatar == "End") {
                inDialogue = false;
                return false;
            
            } else if (avatar == "Decide") {
                isChoosing = true;
                JsonData options = line[0];
                textDisplay.text = "";
                for(int optionsNumber = 0; optionsNumber < options.Count; optionsNumber++) {
                    ActivateButtons(buttons[optionsNumber],options[optionsNumber]);
                }
                if (index==4 || index == 13) ActivateTablet(); 
            }

            if (!isChoosing) {
                StopAllCoroutines();
                textDisplay.text = "";
                if (avatar.ToString() == "Alex") {
                    AvatarDisplay.TalkingMA();
                    AvatarDisplay.NotTalkingFK();
                } else if (avatar.ToString() == "Sie") {
                    AvatarDisplay.TalkingFK();
                    AvatarDisplay.NotTalkingMA();
                } else {
                    AvatarDisplay.NotTalkingFK();
                    AvatarDisplay.NotTalkingMA();
                }
                StartCoroutine(TypeLetters(avatar + ": " + line[0].ToString()));
                Debug.Log(line[0].ToString());
                if (index != 14) DeactivateTablet();
            } 
            index++;
            isChoosing = false;
        }
        return true;
    }

    public void DeactivateButtons() {
        foreach (GameObject button in buttons) {
            button.SetActive(false);
            button.GetComponentInChildren<TextMeshProUGUI>().text = "";
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
    }

    public void ActivateButtons(GameObject button, JsonData choice) {
        AvatarDisplay.NotTalkingMA();
        AvatarDisplay.TalkingFK();
        button.SetActive(true);
        button.GetComponentInChildren<TextMeshProUGUI>().text = "Sie: "+ choice[0][0].ToString();
        button.GetComponent<Button>().onClick.AddListener(delegate { ToDoOnClick(choice); });
    }

    private void ToDoOnClick(JsonData choice) {
        PrintNextSentence();
        DeactivateButtons();
    }

    // CODE TAKEN FROM END 1

    public IEnumerator TypeLetters(string sentence) {
        foreach (char letter in sentence.ToCharArray()) {
            textDisplay.text += letter;
            yield return null;
        }
    } 

    public IEnumerator ChangeSceneWithDelay(string scene) {
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene(scene);
    }

    private void DeactivateTablet() {
        tablet.GetComponent<Animator>().enabled = false;
        tablet.GetComponent<AudioSource>().enabled = false;
    }

    private void ActivateTablet() {
        tablet.GetComponent<Animator>().enabled = true;
        tablet.GetComponent<AudioSource>().enabled = true;
    }

}
