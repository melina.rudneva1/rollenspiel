﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class MonologueManager : MonoBehaviour
{
    public TMP_Text textDisplay;
    private Queue<string> sentences;
    [TextArea(2,15)]
    public string[] monologue;
    public string nextScene=null;
    public GameObject tablet;


    void Start() {
        tablet.GetComponent<AudioSource>().enabled = false;
        sentences = new Queue<string>();
        StartMonologue(monologue);
    }


    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) PrintNextSentence();
    } 


    public void StartMonologue(string[] monologue) {
        sentences.Clear();
        foreach (string sentence in monologue) {
            sentences.Enqueue(sentence);
        }
        PrintNextSentence();
    }


    public void PrintNextSentence() {
        if (sentences.Count == 0) {
            EndMonologue();
            return;
        }
        if (sentences.Count == 4) tablet.GetComponent<AudioSource>().enabled = true;
        string sentence = sentences.Dequeue();
        Debug.Log(sentence);
        textDisplay.text = "";
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }


    public void EndMonologue() {
        Debug.Log("End of scene");
        if (nextScene!=null) SceneManager.LoadScene(nextScene);
    }


    IEnumerator TypeSentence (string sentence) {
        foreach (char letter in sentence.ToCharArray()) {
            textDisplay.text += letter;
            yield return null;
        }
    }
    

} 
